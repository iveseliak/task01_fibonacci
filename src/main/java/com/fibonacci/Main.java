package com.fibonacci;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        try {
            System.out.println("Введіть інтервал чисел від 0 до 10000");
            Scanner scanner = new Scanner(System.in);
            Fibonacci fibonacci = new Fibonacci(scanner.nextInt(), scanner.nextInt());

            if (fibonacci.getFrom() < 0 || fibonacci.getTo() > 10000) {
                System.out.println("Введено не коректне чиcло");

            } else {
                fibonacci.menu();
                fibonacci.setPoint();
            }
        } catch (
                InputMismatchException exception) {
            System.out.println("Введено не число");

        }


    }


}
