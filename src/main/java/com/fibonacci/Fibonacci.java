package com.fibonacci;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Veseliak Ivan
 */

public class Fibonacci {
    /**
     * interval begin from number
     */
    private int from;
    /**
     * last number in interval
     */
    private int to;

    /**
     * This is a constructor to initialize numbers interval
     * @param from
     * @param to
     */
    public Fibonacci(int from, int to) {
        this.from = from;
        this.to = to;
    }

    /**
     * show interval odd and even numbers
     */
    public void printOddAndEvenNumbers() {
        for (int i = this.from; i <= this.to; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ";");
            }

        }

        System.out.println(" ");

        for (int j = this.to; j >= this.from; j--) {
            if (j % 2 != 0) {
                System.out.print(j + ";");
            }

        }
        System.out.println(" ");
    }

    /**
     * show sum number of interval
     */
    public void printSumAllNumbers() {
        int sum = 0;
        for (int i = this.from; i <= this.to; i++) {
            sum += i;

        }
        System.out.println("Сума всіх чисул в даному інтервалі = " + sum);
    }

    /**
     * show biggest odd and even numbers
     */
    public void biggestOddAndEvenFibNumbers() {
        int fib[] = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946};
        int fibOdd = 0;
        int fibEven = 0;
        for (int i = 0; i < fib.length; i++) {
            if (fib[i] % 2 == 0 && fib[i] <= this.to) {
                fibOdd = fib[i];
            }

            if (fib[i] % 2 != 0 && fib[i] <= this.to) {
                fibEven = fib[i];
            }
        }

        System.out.println("Найбільше парне число фібоначчі =" + fibOdd);
        System.out.println("Найбільше непарне число фібоначчі =" + fibEven);
    }

    /**
     * show percentage fibbonachi numbers in interval
     */
    public void percentageFibNumbers() {
        int fib[] = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946};
        int countFibNumbers = 0;
        int countNumbers = this.to - this.from;
        for (int i = 0; i < fib.length; i++) {
            if (fib[i] >= from && fib[i] <= to) {
                countFibNumbers++;
            }
        }
        System.out.println(countFibNumbers);
        System.out.println(countNumbers);

        double percentage = countFibNumbers * 100 / countNumbers;
        System.out.println(percentage);
        System.out.println("Процент чисел фібоначчі=" + percentage + " %");
    }

    /**
     * navigation method
     */
    public void setPoint() {
        try {
            System.out.println("Вкажіть пункт меню!");
            Scanner scanner = new Scanner(System.in);
            Integer menuPoint = scanner.nextInt();

            if (menuPoint == 1) {
                printOddAndEvenNumbers();
                setPoint();
            } else if (menuPoint == 2) {
                printSumAllNumbers();
                setPoint();
            } else if (menuPoint == 3) {
                biggestOddAndEvenFibNumbers();
                setPoint();
            } else if (menuPoint == 4) {
                percentageFibNumbers();
                setPoint();
            } else if (menuPoint == 5) {
                return;

            } else {
                System.out.println("Не коректно вказано пункт меню!");
                setPoint();

            }

    }catch(
    InputMismatchException exception)

    {
        System.out.println("Введено не число");
        setPoint();
    }

}

    /**
     *
     * menu
     */
    public void menu() {
        System.out.println("Виберіть один з пунктів");
        System.out.println("1. Вказати парні числа від початку і до кінця інтервалу та непарні числа від кінця інтервалу до початку");
        System.out.println("2. Вказати суму всіх чисел");
        System.out.println("3. Вибрати найбільше парне та непарне число фібоначчі з інтервалу");
        System.out.println("4. Вказати процент чисел фібоначчі в даному інтервалі.");
        System.out.println("5. Вихід");
    }

    /**
     * show interval first number
     * @return interval first number
     */
    public int getFrom() {
        return from;
    }

    /**
     * To set a first first number in interval
     * @param from a first number
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * show interval last number
     * @return interval last number
     */
    public int getTo() {
        return to;
    }

    /**
     * To set a last number in interval
     * @param to a last number
     */
    public void setTo(int to) {
        this.to = to;
    }
}
